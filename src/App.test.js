import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const colourButton = screen.getByRole('button', { name: 'Change to blue'});
  expect(colourButton).toHaveStyle({ backgroundColor: 'red' });
});

test('button turn blue when clicked', () => {
  render(<App />);
  const colourButton = screen.getByRole('button', { name: 'Change to blue'});
  expect(colourButton).toHaveStyle({ backgroundColor: 'red' });
  //Click button
  fireEvent.click(colourButton);
  expect(colourButton).toHaveStyle({ backgroundColor: 'blue' });
  //Expect the text to change after click
  expect(colourButton.textContent).toBe('Change to red');
});

test('intial conditions', () => {
  render(<App />);
  //check that the button starts out enabled
  const colourButton = screen.getByRole('button', { name: 'Change to blue'});
  expect(colourButton).toBeEnabled();

  //check that the checkbox starts out unchecked
  const checkbox = screen.getByRole('checkbox');
  expect(checkbox).not.toBeChecked();

});

/*
test('text conditions', () => {
  render(<App />);
  //check the placeholder
  const textBox = screen.getByRole('text', { name: 'name'});
  expect(textBox.textContent).toStrictEqual('Enter name');
}); */

test('GraphQLAPI testing', () => {
  //Test the GraphQL api
  const arkP = {
    "ethereum": {
      "dexTrades": [
        {
          "protocol": "Uniswap v2",
          "started": "2020-05-05"
        }
      ]
    }
  };

  return fetch('https://graphql.bitquery.io/', {
		method: 'POST',
		headers: {"Content-Type": "application/json",'X-API-KEY': 'BQY6Wtz0YIJ2AwIEJsY8cxGTcIX4pyza'},
		body: JSON.stringify({ query: 
			`query{
        ethereum(network: ethereum) {
          dexTrades(protocol: {is: "Uniswap v2"}) {
            protocol
            started: minimum(of: date)
          }
        }
      }` 
		}),
	})
	.then(res => res.json())
	// The test condition itself
	.then(res => expect(res.data).toStrictEqual(arkP));
})