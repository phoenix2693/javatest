import { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
const [buttonColor,setButtonColor] = useState('red');
const newButtonColor = buttonColor === 'red' ? 'blue' : 'red';

  return (
    <div className="App">
      <label htmlFor="name">Enter Name:</label>
      <input type="text" id="name" name="name" placeholder="Enter name"/>
      <br></br>
      <label for="password">Enter password:</label>
      <input type="text" id="password" name="password" placeholder="Enter name"/>
      <br></br>
      <button style={{backgroundColor: buttonColor}} onClick= {() => setButtonColor(newButtonColor)}>Change to {newButtonColor}</button>
      <br></br>
      <input type="checkbox"/>
    </div>
  );
}

export default App;
